(in-package #:syscat-cl-utilities)


(defclass asdf-platform-permutation ()
  ((cl-type :initarg :cl-type
            :accessor cl-type
            :type string
            :initform (lisp-implementation-type)
            :documentation "The implementation of Common Lisp on which this class was instantiated, e.g. SBCL")
   (cl-version :initarg :cl-version
               :accessor cl-version
               :type string
               :initform (lisp-implementation-version)
               :documentation "The version of the Common Lisp implementation on which this class was instantiated, e.g. 2.3.9.nixos")
   (cpu-arch :initarg :cpu-arch
             :accessor cpu-arch
             :type string
             :initform (machine-type)
             :documentation "The CPU architecture on which this class was instantiated, e.g. X86-64")
   (os-type :initarg :os-type
            :accessor os-type
            :type string
            :initform (software-type)
            :documentation "The OS on which this class was instantiated, e.g. Linux")
   (os-version :initarg :os-version
               :accessor os-version
               :type string
               :initform (software-version)
               :documentation "The version of the OS on which this class was instantiated, e.g. 6.1.61"))
  (:documentation "Identify the key characteristics of the platform on which a given ASDF system was compiled or tested."))

(defmethod print-object ((obj asdf-platform-permutation) stream)
  (print-unreadable-object (obj stream :type t :identity t))
  (with-slots (cl-type cl-version cpu-arch os-type os-version) obj
    (format stream "~&Common Lisp implementation: ~A:~A~%Operating system: ~A:~A~%CPU architecture: ~A~%"
            cl-type cl-version os-type os-version cpu-arch)))
