;    Copyright James Fleming <james@electronic-quill.net>
;

(asdf:defsystem #:syscat-cl-utilities
  :author "James Fleming <james@electronic-quill.net>"
  :description "CL utilities for Syscat."
  :depends-on (#:log4cl
               #:drakma
               #:rg-client
               #:shasht)
  :components ((:file "package")
               (:file "model-asdf-dependencies" :depends-on ("package"))
               (:file "import-spdx-licenses" :depends-on ("package"))
               (:file "utilities" :depends-on ("package"))))
