(in-package #:syscat-cl-utilities)

;; Walk an ASDF system's dependencies, and make sure they're all in Syscat as libraries, plus
;; - connection to the relevant SPDX license definition
;; - connection to their author
;; - bidirectional links between each dependency pair


;; Hints to self:
;; - asdf:find-system
;; - asdf:system-depends-on
;; - asdf:system-license

(defun import-dependencies (rg-server packagename)
  "Walk a package's dependencies, and ensure they're all imported into Syscat.
  Assumes the parent package is an application, unless :library-p is set to t.
  Assumes all its dependencies are libraries."
  (declare (type rg-client:server rg-server)
           (type (or string keyword) packagename))
  (log:debug "Ensuring CommonLisp is recorded as a programming language.")
  (rg-client:ensure-resource-exists rg-server "ProgrammingLanguages" "CommonLisp")
  (let ((sysdef (asdf:find-system packagename)))
    (log:debug "Attempting to import dependencies for system ~A" (asdf:primary-system-name sysdef))
    (let ((system-name (asdf:primary-system-name sysdef))
          (license (asdf:system-license sysdef))
          (author (asdf:system-author sysdef)))
      (log:debug "Attempting to update Syscat with system ~A, with license ~A and author '~A'"
                 system-name
                 license
                 author)
      (rg-client:post-request
        rg-server
        "ASDFsystems"
        system-name
        `(("uid" . ,system-name))))))
