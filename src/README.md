Syscat Common-Lisp Utilities
============================

Surprisingly, this is a collection of utility stuff for Syscat, written in Common Lisp.


# import-spdx-licenses

Creates an `SPDXlicenses` entry for each definition in the SPDX repo at `https://github.com/spdx/license-list-data`.

Works by iterating over the JSON definition files in the `json/details` subdirectory of the SPDX repo at `https://github.com/spdx/license-list-data`.


## Usage

- Download and unzip that data somewhere.
- Start your REPL.
- `(asdf:load-system :syscat-cl-utilities)`
- Create an instance of `rg-client:server`
    - E.g: `(defvar *server* (make-instance 'rg-client:server :hostname "192.0.2.1" :address "192.0.2.1" :port 4950))`
- Invoke `syscat-cl-utilities:import-spdx-licenses`, passing it that object and the path to the SPDX license data, so you only need to specify the path down to `license-list-data/`, but you do need to end the path with a forward-slash:
    - `(syscat-cl-utilities:import-spdx-licenses *server* "/data/SPDX/license-list-data/")`
    - Note that it automatically appends "/json/details/" to that path.
    - If authentication is required, also specify the :username and :password parameters.
